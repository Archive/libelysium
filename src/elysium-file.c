#include "elysium-file.h"

gchar * elysium_pixmap_file (gchar * filename) {
  gchar * blah;
  gint i;
  gchar * syspaths[] = {
    EPIXMAPDIR,
    "/usr/local/share/pixmaps/",
    "/opt/gnome2/share/pixmaps/",
    "/usr/share/pixmaps/",
    NULL
  };

  blah = g_strconcat (g_get_home_dir (), "/.gnome2/pixmaps/", filename, NULL);
  if (g_file_exists (blah)) {
    return blah;
  }
  blah = g_strconcat (g_get_home_dir (), "/pixmaps/", filename, NULL);
  if (g_file_exists (blah)) {
    return blah;
  }
  blah = g_strconcat (g_get_home_dir (), "/share/pixmaps/", filename, NULL);
  if (g_file_exists (blah)) {
    return blah;
  }
  for (i = 0; syspaths && syspaths[i]; i++) {
    blah = g_strconcat (syspaths[i], filename, NULL);
    if (g_file_exists (blah)) {
      return blah;
    }
  }
  g_free (blah);
  return filename;
}
