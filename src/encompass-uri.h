/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  htmlurl.h

    Copyright (C) 1999 Helix Code, Inc.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    Authors: Ettore Perazzoli (ettore@helixcode.com)
             Rodney Dawes (dobey@free.fr)
							
*/

#ifndef _ENCOMPASS_URI_H
#define _ENCOMPASS_URI_H

#include <glib.h>

typedef struct {
	gchar *protocol;
	gchar *username;
	gchar *password;
	gchar *hostname;
	guint16 port;
	gchar *path;
	gchar *reference;
} EncompassURI;

typedef enum {
	ENCOMPASS_URI_DUP_ALL = 0,
	ENCOMPASS_URI_DUP_NOPROTOCOL = 1 << 0,
	ENCOMPASS_URI_DUP_NOUSERNAME = 1 << 1,
	ENCOMPASS_URI_DUP_NOPASSWORD = 1 << 2,
	ENCOMPASS_URI_DUP_NOHOSTNAME = 1 << 3,
	ENCOMPASS_URI_DUP_NOPORT = 1 << 4,
	ENCOMPASS_URI_DUP_NOPATH = 1 << 5,
	ENCOMPASS_URI_DUP_NOREFERENCE = 1 << 6,
	ENCOMPASS_URI_DUP_NOCGIARGS = 1 << 7
} EncompassURIDupFlags;

EncompassURI *encompass_uri_new (const gchar *s);
void encompass_uri_destroy (EncompassURI *url);
EncompassURI *encompass_uri_dup (const EncompassURI *url,
				 EncompassURIDupFlags flags);

void encompass_uri_set_protocol (EncompassURI *url, const gchar *protocol);
void encompass_uri_set_username (EncompassURI *url, const gchar *username);
void encompass_uri_set_password (EncompassURI *url, const gchar *password);
void encompass_uri_set_hostname (EncompassURI *url, const gchar *password);
void encompass_uri_set_port (EncompassURI *url, gushort port);
void encompass_uri_set_path (EncompassURI *url, const gchar *path);
void encompass_uri_set_reference (EncompassURI *url, const gchar *reference);

const gchar *encompass_uri_get_protocol (const EncompassURI *url);
const gchar *encompass_uri_get_username (const EncompassURI *url);
const gchar *encompass_uri_get_password (const EncompassURI *url);
const gchar *encompass_uri_get_hostname (const EncompassURI *url);
gushort encompass_uri_get_port (const EncompassURI *url);
const gchar *encompass_uri_get_path (const EncompassURI *url);
const gchar *encompass_uri_get_reference (const EncompassURI *url);

gchar *encompass_uri_to_string (const EncompassURI *url);

EncompassURI *encompass_uri_append_path (const EncompassURI *url, const gchar *path);

EncompassURI * encompass_uri_concat (gchar * url, EncompassURI * base);

#endif /* _ENCOMPASS_URI_H_ */
