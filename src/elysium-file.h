#ifndef _ELYSIUM_FILE_H_
#define _ELYSIUM_FILE_H_

#include <glib.h>
#include <libgnome/gnome-util.h>

gchar * elysium_pixmap_file (gchar * filename);

#endif
